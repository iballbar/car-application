package com.bluebik.car.musclecar;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Copyright © 2016 Bluebik Group.
 * Created by khakhanat on 24/10/2017 AD.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MuscleCarResourceTest {

    @MockBean
    MuscleCarService muscleCarService;

    @Autowired
    MuscleCarResource muscleCarResource;

    @Before
    public void setUp() throws Exception {
        Mockito.when(muscleCarService.getCar(1)).thenReturn(new MuscleCar("1", "2", "3", "4"));
        Mockito.when(muscleCarService.listAllCars()).thenReturn(new ArrayList<>());
    }

    @Test
    public void getMuscleCar() throws Exception {
        ResponseEntity<Map<String, Object>> responseEntity = muscleCarResource.getMuscleCar(1);
        Map<String, Object> body = responseEntity.getBody();
        MuscleCar muscleCar = (MuscleCar) body.get("car");
        Assert.assertEquals("1", muscleCar.getCarBrand());
        Assert.assertEquals("2", muscleCar.getCarModel());
        Assert.assertEquals("3", muscleCar.getHorsepower());
        Assert.assertEquals("4", muscleCar.getCarEngine());
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void getMuscleCarNotfound() throws Exception {
        ResponseEntity<Map<String, Object>> responseEntity = muscleCarResource.getMuscleCar(2);
        Map<String, Object> body = responseEntity.getBody();
        MuscleCar muscleCar = (MuscleCar) body.get("car");
        Assert.assertNull(muscleCar);
        Assert.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void deleteMuscleCar() throws Exception {
        ResponseEntity<Map<String, Object>> responseEntity = muscleCarResource.deleteMuscleCar(1);
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void addCar() throws Exception {
        ResponseEntity<Map<String, Object>> responseEntity = muscleCarResource.addCar(
            new MuscleCar("brand", "model", "power", "engine")
        );
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void listAllCars() throws Exception {
        ResponseEntity<Map<String, Object>> listResponseEntity = muscleCarResource.listAllCars();
        Map<String, Object> body = listResponseEntity.getBody();
        List<MuscleCar> cars = (List<MuscleCar>) body.get("cars");
        Assert.assertEquals(0, cars.size());
        Assert.assertEquals(HttpStatus.OK, listResponseEntity.getStatusCode());
    }

    @Test
    public void updateCar() throws Exception {
        ResponseEntity<Map<String, Object>> responseEntity = muscleCarResource.updateCar(1,
            new MuscleCar("brand", "model", "power", "engine")
        );
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

}