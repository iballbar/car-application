package com.bluebik.car.musclecar;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MuscleCarServiceTest {

    @MockBean
    MuscleCarDaoImpl muscleCarDao;

    @Autowired
    MuscleCarService muscleCarService;

    @Before
    public void setUp() throws Exception {
        Mockito.when(muscleCarDao.getCar(1)).thenReturn(new MuscleCar("1", "2", "3", "4"));
    }

    @Test
    public void testGetCar() throws Exception {
        MuscleCar muscleCar = muscleCarService.getCar(1);
        Assert.assertEquals("1", muscleCar.getCarBrand());
        Assert.assertEquals("2", muscleCar.getCarModel());
        Assert.assertEquals("3", muscleCar.getHorsepower());
        Assert.assertEquals("4", muscleCar.getCarEngine());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCar_ThrowException() throws Exception {
        muscleCarService.getCar(0);
    }


    @Test
    public void testRemoveCar() throws Exception {
        muscleCarService.removeCar(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveCar_ThrowException() throws Exception {
        muscleCarService.removeCar(0);
    }


    @Test
    public void testListAllCars() throws Exception {
        List<MuscleCar> mockCars = new ArrayList<MuscleCar>();
        mockCars.add(new MuscleCar("brand", "model", "power", "engine"));
        mockCars.add(new MuscleCar("brand", "model", "power", "engine"));

        Mockito.when(muscleCarDao.listAllCars()).thenReturn(mockCars);
        List<MuscleCar> cars = muscleCarService.listAllCars();
        Assert.assertEquals(mockCars.size(), cars.size());
    }

    @Test
    public void testListAllCarsNull() throws Exception {
        Mockito.when(muscleCarDao.listAllCars()).thenReturn(new ArrayList<>());
        List<MuscleCar> cars = muscleCarService.listAllCars();
        Assert.assertNull(cars);
    }



    @Test
    public void testAddCar() throws Exception {
        MuscleCar car = new MuscleCar("brand", "model", "power", "engine");
        muscleCarService.addCar(car);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddCar_ThrowException() throws Exception {
        muscleCarService.addCar(null);
    }

    @Test
    public void testUpdateCar() throws Exception {
        muscleCarService.updateCar(1,
                new MuscleCar("brand", "model", "power", "engine")
                );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateCar_ThrowException() throws Exception {
        muscleCarService.updateCar(0,
                null
        );
    }
}