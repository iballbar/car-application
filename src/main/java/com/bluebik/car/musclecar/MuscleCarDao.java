package com.bluebik.car.musclecar;

import java.util.List;

/**
 * Copyright © 2016 Bluebik Group.
 * Created by khakhanat on 24/10/2017 AD.
 */
public interface MuscleCarDao {

    public MuscleCar getCar(int id);

    public void removeCar(int id);

    public void addCar(MuscleCar muscleCar);

    public void updateCar(int id, MuscleCar muscleCar);

//    public List<Map<String, Object>> listAllCars();
    public List<MuscleCar> listAllCars();

}
