package com.bluebik.car.musclecar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Copyright © 2016 Bluebik Group.
 * Created by khakhanat on 24/10/2017 AD.
 */
@Service
public class MuscleCarService {

    private MuscleCarDao muscleCarDao;

//    public MuscleCarService() {
//        this.muscleCarDao = new MuscleCarDaoImpl();
//    }
    @Autowired
    public MuscleCarService(MuscleCarDao muscleCarDao) {
        this.muscleCarDao = muscleCarDao;
    }

    @Transactional
    public MuscleCar getCar(int id) {
        if (id <= 0) {
            throw new IllegalArgumentException("ID can not be 0 or <0");
        }
        return muscleCarDao.getCar(id);
    }

    @Transactional
    public void removeCar(int id) {
        if (id <= 0) {
            throw new IllegalArgumentException("ID can not be 0 or <0 or this id do not exist");
        }
        muscleCarDao.removeCar(id);
    }

    @Transactional
    public List<MuscleCar> listAllCars() {

        List<MuscleCar> result = muscleCarDao.listAllCars();
        if (result.size() > 0) {
            return result;
        } else {
            return null;
        }
    }

    @Transactional
    public void addCar(MuscleCar muscleCar) {
        if (muscleCar == null) {
            throw new IllegalArgumentException("The passed object cna not be null.");
        }
        muscleCarDao.addCar(muscleCar);
    }

    @Transactional
    public void updateCar(int id, MuscleCar muscleCar) {
        if (id <= 0 && muscleCar == null) {
            throw new IllegalArgumentException("The passed object cna not be null.");
        }
        muscleCarDao.updateCar(id, muscleCar);
    }

}
