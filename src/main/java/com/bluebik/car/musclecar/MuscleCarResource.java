package com.bluebik.car.musclecar;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright © 2016 Bluebik Group.
 * Created by khakhanat on 24/10/2017 AD.
 */
@RestController
@RequestMapping(value = "/api/cars")
public class MuscleCarResource {

    private MuscleCarService muscleCarService;

    @Autowired
    public MuscleCarResource(MuscleCarService muscleCarService) {
        this.muscleCarService = muscleCarService;
    }

    @RequestMapping(value = "/get-car/{id}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getMuscleCar(@PathVariable("id") int id) {

        Map<String, Object> map = new HashMap<>();
        try {
            MuscleCar muscleCar = muscleCarService.getCar(id);
            if (muscleCar != null) {
                map.put("message", "success");
                map.put("car", muscleCar);
                return ResponseEntity.status(HttpStatus.OK).body(map);
            } else {
                map.put("message", HttpStatus.NOT_FOUND.getReasonPhrase());
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(map);
            }

        } catch (Exception e) {
            map.put("message", HttpStatus.BAD_REQUEST.getReasonPhrase());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(map);
        }

    }

    @RequestMapping(value = "/delete-car/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Map<String, Object>> deleteMuscleCar(@PathVariable("id") int id) {

        Map<String, Object> map = new HashMap<>();
        try {
            muscleCarService.removeCar(id);
            map.put("message", "success");
            return ResponseEntity.status(HttpStatus.OK).body(map);
        } catch (Exception e) {
            map.put("message", HttpStatus.BAD_REQUEST.getReasonPhrase());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(map);
        }

    }

    @RequestMapping(value = "/add-car", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> addCar(@RequestBody MuscleCar muscleCar) {

        Map<String, Object> map = new HashMap<>();
        try {
            muscleCarService.addCar(muscleCar);
            map.put("message", "success");
            map.put("car", muscleCar);
            return ResponseEntity.status(HttpStatus.OK).body(map);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("message", HttpStatus.BAD_REQUEST.getReasonPhrase());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(map);
        }
    }

    @RequestMapping(value = "/cars", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> listAllCars() {

        Map<String, Object> map = new HashMap<>();
        try {
            List<MuscleCar> result = muscleCarService.listAllCars();
            map.put("message", "success");
            map.put("cars", result);
            return ResponseEntity.status(HttpStatus.OK).body(map);
        } catch (Exception e) {
            map.put("message", HttpStatus.BAD_REQUEST.getReasonPhrase());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(map);
        }
    }

    @RequestMapping(value = "/update-car/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Map<String, Object>> updateCar(@PathVariable("id") int id, @RequestBody MuscleCar muscleCar) {

        Map<String, Object> map = new HashMap<>();
        try {
            muscleCarService.updateCar(id, muscleCar);
            map.put("message", "success");
            map.put("car", muscleCar);
            return ResponseEntity.status(HttpStatus.OK).body(map);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("message", HttpStatus.BAD_REQUEST.getReasonPhrase());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(map);
        }
    }

}
