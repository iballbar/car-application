package com.bluebik.car.musclecar;



import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Copyright © 2016 Bluebik Group.
 * Created by khakhanat on 24/10/2017 AD.
 */
@Repository
public class MuscleCarDaoImpl implements MuscleCarDao {

    private EntityManager entityManager;

    // set up constructor injection
    @Autowired
    public MuscleCarDaoImpl(EntityManager theEntityManager) {
        entityManager = theEntityManager;
    }
    
    @Override
    public MuscleCar getCar(int id) {

        Session currentSession = entityManager.unwrap(Session.class);

        return currentSession.get(MuscleCar.class, id);

    }

    @Override
    public void removeCar(int id) {
        Session currentSession = entityManager.unwrap(Session.class);

        Query theQuery =
                currentSession.createQuery(
                        "delete from MuscleCar where car_id=:carId");
        theQuery.setParameter("carId", id);

        theQuery.executeUpdate();
    }

    @Override
    public void addCar(MuscleCar muscleCar) {
        Session currentSession = entityManager.unwrap(Session.class);

        currentSession.save(muscleCar);
    }

    @Override
    public void updateCar(int id, MuscleCar muscleCar) {
        Session currentSession = entityManager.unwrap(Session.class);

        currentSession.update(muscleCar);
    }

    @Override
    public List<MuscleCar> listAllCars() {
        Session currentSession = entityManager.unwrap(Session.class);
        Query theQuery = currentSession.createQuery("from MuscleCar");

        return (List<MuscleCar>) theQuery.list();
    }

}
